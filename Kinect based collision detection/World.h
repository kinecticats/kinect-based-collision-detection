//////////////////////////////////////////////////////////////////////////////////////////////
//
// Kinect based collision detection
// For "Praktikum: Computerspiele / Special Effects"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a kinect based collision detection between virtual objects 
// and real persons standing in front of the kinect. To achieve the goal, the project is using
// following sources:
//						- GLUT
//						- OpenGL
//						- Bullet Physics
//						- Microsoft Kinect SDK
//
// Credit goes to Rob Bateman for his opengl_ui package 
// And to Erwin Coumans for the C++ Bullet Physics Library.
//
// Authors are Adrian Czarkowski and Holger Honz
// See us also on Bitbucket, where our project is hosted:
// https://bitbucket.org/kinecticats/kinect-based-collision-detection/
//
//////////////////////////////////////////////////////////////////////////////////////////////


/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it freely,
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/


#ifndef WORLD
#define WORLD

#include "Button.h"
#include "Kinect.h"
#include "GlutDemoApplication.h"
#define PlatformDemoApplication GlutDemoApplication
#include "LinearMath/btAlignedObjectArray.h"

class btBroadphaseInterface;
class btCollisionShape;
class btOverlappingPairCache;
class btCollisionDispatcher;
class btConstraintSolver;
class btDefaultCollisionConfiguration;
struct btCollisionAlgorithmCreateFunc;


/**
 * World Class:
 * Creates a 3D space
 */
class World : public PlatformDemoApplication
{
	// Array to store the collision shapes, for deletion/cleanup
	btAlignedObjectArray<btCollisionShape*>	m_collisionShapes;

	// Needed to calculate collisions of bounding-boxes
	btBroadphaseInterface*	m_broadphase;

	// Collision dispatcher to process the collisions occuring
	btCollisionDispatcher*	m_dispatcher;

	// Default constraint solver to keep relationship between objects correct
	btConstraintSolver*	m_solver;

	// Collision configuration contains default setup for memory, collision setup
	btDefaultCollisionConfiguration* m_collisionConfiguration;

	// The camera distance
	btScalar cameraDistance;

	// The pointer to a Kinect
	Kinect* ptr_kinect;

	// An internal clock for time measuring
	btClock firstClock;

	// An internal clock for time measuring
	btClock secondClock;

	// Array storing all indices of real objects
	int reals[20];

	// Array storing all indices of wall objects
	int walls[30];

	// The current window width
	int windowWidth;

	// The current window height
	int windowHeight;

	// Stores the number of dodged balls in the sphere mode
	int dodged;

	// Whether there was a collision in the sphere mode
	bool collision;

	// The time needed to destroy the wall
	unsigned long destroyTime;

	// Number of the latest sphere
	int lastSphere;

	// The buttons
	Button* Mode1;
	Button* Mode2;
	Button* Mode3;
	Button* Score;
	Button* Info;
	Button* Exit;
	Button* Back;

	// Flags which mode is currently active
	// 0 => menu
	// 1 => shootSphere
	// 2 => createWall
	// 3 => fallingSnow
	int mode;

public:
	// The constructor
	World();

	// The destructor
	virtual ~World()
	{
		// Stop physics
		exitPhysics();

		// Delete the clocks
		delete &firstClock;
		delete &secondClock;

		// Delete the buttons
		delete Mode1;
		delete Mode2;
		delete Mode3;
		delete Score;
		delete Info;
		delete Exit;
		delete Back;
	}

	// Associate a kinect with the world
	void associateKinect(Kinect* kinect);

	// Initializes physics simulations
	void initPhysics();

	// Stops physics simulations
	void exitPhysics();

	// Steps the simulation and renders the screen
	virtual void clientMoveAndDisplay();

	// Displays buttons on the window
	void displayButtons();

	// Handles keyboard input
	virtual void keyboardCallback(unsigned char key, int x, int y);

	// Handles mouse input
	virtual void mouseFunc(int button, int state, int x, int y);

	// Renders the 3D scene after altering the window
	virtual void displayCallback();

	// Computes a random float between 0.0 and x
	float random(float x);

	// Shoots a little ball on the viewer
	void shootSphere();

	// Creates a Wall of box shaped rigid bodies
	void createWall();

	// Creates falling snow
	void fallingSnow();

	// Initializes the skeleton
	void initSkeleton();

	// Moves the joints based on the data in the given array
	void moveSkeleton(btVector3 coords[], int coordNum);

	// Resets the init state of the world
	virtual void clientResetScene();

	// Draws the info text
	void drawInfoText();

	// Checks for a collision of a sphere and the skeleton
	bool collisionOccurred();

	// Checks if the wall is destroyed
	bool wallDestroyed();

	// Initializes a new world
	static DemoApplication* Create()
	{
		World* world = new World;
		world->myinit();
		world->initPhysics();
		return world;
	}
};


#endif WORLD