//////////////////////////////////////////////////////////////////////////////////////////////
//
// Kinect based collision detection
// For "Praktikum: Computerspiele / Special Effects"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a kinect based collision detection between virtual objects 
// and real persons standing in front of the kinect. To achieve the goal, the project is using
// following sources:
//						- GLUT
//						- OpenGL
//						- Bullet Physics
//						- Microsoft Kinect SDK
//
// Credit goes to Rob Bateman for his opengl_ui package 
// And to Erwin Coumans for the C++ Bullet Physics Library.
//
// Authors are Adrian Czarkowski and Holger Honz
// See us also on Bitbucket, where our project is hosted:
// https://bitbucket.org/kinecticats/kinect-based-collision-detection/
//
//////////////////////////////////////////////////////////////////////////////////////////////


/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it freely,
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/


#include <ctime>
#include <string>
#include "World.h"
#include "GlutStuff.h"
#include "GLDebugFont.h"
#include "btBulletDynamicsCommon.h" // Contains most common include files
#include <iostream>					// For debugging purpose on the console
#include "GLDebugDrawer.h"			// For debugging purpose on the screen

// Uncomment to use the predefined keyboard callback
//#define GLUT_KEYBOARD_INPUT

// Create a debug drawer
static GLDebugDrawer	sDebugDrawer;

// Define the different modes
enum modes {menu, sphere, wall, snow};


/**
* Initializes the 3D scene,
* sets the camera distance
*/
World::World()
{
	// Store the current window size
	windowWidth = 1024;
	windowHeight = 600;

	// Sets the debug mode to allow debug text
	setDebugMode(btIDebugDraw::DBG_DrawText + btIDebugDraw::DBG_NoHelpText);

	// Defines the initial camera distance
	cameraDistance = btScalar(50.);

	// Sets the initial camera distance
	setCameraDistance(cameraDistance);

	// Initialize the internal flags
	destroyTime = 0;
	lastSphere = 0;
	dodged = 0;
	mode = 0;

	// Initialize the collision flag with false
	collision = false;

	// Set the seed for the random value generator
	srand(time(NULL));

	// Initializes the buttons
	Mode1 = new Button(0, 0, 0, -150, 400, 50, 0, 5, false, "Mode 1: Dodge the ball!");
	Mode2 = new Button(0, 0, 0, -50, 400, 50, 0, 5, false,  "Mode 2: Punch the wall!");
	Mode3 = new Button(0, 0, 0, 50, 400, 50, 0, 5, false,   "Mode 3: Let it snow    ");
	Score = new Button(0, 0, 50, -100, 400, 50, 0, 4, false, "Score");
	Info  = new Button(0, 0, 50, 50, 600, 50, 0, 1, false, "Hover over a button to see what it does");
	Exit  = new Button(0, 0, 0, 150, 400, 50, 0, 5, false,  "          Exit         ");
	Back  = new Button(0, 0, -450, -100, 400, 50, 0, 3, false, "Back to main menu");
}


/**
 * Associate a kinect with the world
 */
void World::associateKinect(Kinect* kinect)
{
	ptr_kinect = kinect;
}


/**
 * Computes a random float between 0.0 and x
 * @param x: float to set the maximum
 * @return A random float
 */
float randomFloat(float x) {
	return (((float)(rand())) / ((float)(RAND_MAX / x)));
}


/**
* Responsible for rendering
* and stepping the 3D scene
*/
void World::clientMoveAndDisplay()
{
	// OpenGL: clears buffers to preset values
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Check if the world still exists
	if (m_dynamicsWorld)
	{
		// Step the simulation in fixed steps
		m_dynamicsWorld->stepSimulation(1. / 60., getDeltaTimeMicroseconds() / 1000000.f);

		// Optional but useful: debug drawing
		m_dynamicsWorld->debugDrawWorld();
	}

	// If in the main menu
	if (mode == menu)
	{
		// Execute every 25 ms
		if (secondClock.getTimeMilliseconds() >= 25)
		{
			// Rotate the 3D scene
			m_azi -= 0.25f; if (m_azi < 0) m_azi += 360; updateCamera();

			// Reset the clock
			secondClock.reset();
		}

		// Check the Kinect status
		if (ptr_kinect->kinectStatus == S_OK)
		{
			// Change the label of the score button
			Score->setLabel("Kinect connected");
		}
		else
		{
			// Change the label of the score button
			Score->setLabel("Kinect not connected");

			// Update the kinect status
			ptr_kinect->kinectStatus = ptr_kinect->initKinect();
		}

		// Check if the buttons need to be highlighted
		Mode1->ButtonPassive(this->m_mouseOldX, this->m_mouseOldY);
		Mode2->ButtonPassive(this->m_mouseOldX, this->m_mouseOldY);
		Mode3->ButtonPassive(this->m_mouseOldX, this->m_mouseOldY);
		Score->ButtonPassive(this->m_mouseOldX, this->m_mouseOldY);
		Exit->ButtonPassive(this->m_mouseOldX, this->m_mouseOldY);

		// Check which info text is to be displayed
		drawInfoText();
	} 
	else
	{
		// Move the skeleton according to the kinect data every 1 ms
		if (firstClock.getTimeMilliseconds() >= 1)
		{
			// There are 20 different joints per skeleton
			const int jointsNum = 20;

			// Update the skeleton data
			if (ptr_kinect->kinectStatus == S_OK)
			{
				ptr_kinect->getNewSkeletonFrame();
				moveSkeleton(ptr_kinect->skeleton, jointsNum);
			}

			// Reset the clock
			firstClock.reset();
		}

		// Check if the button needs to be highlighted
		Back->ButtonPassive(this->m_mouseOldX, this->m_mouseOldY);
	}

	// Mode: Dogdge the Ball
	if (mode == sphere)
	{
		// Call the function "shootSphere" every 2 sec
		if (secondClock.getTimeMilliseconds() >= 2000 && !collision)
		{
			// Change the label of the score button
			std::string score = "Dodged balls: " + std::to_string(dodged);
			Score->setLabel(score);

			// Increment the dodged counter
			// Assumption: A ball not colliding is a dodged ball (saves performance)
			dodged++;

			// Shoot a sphere
			shootSphere();

			// Reset the clock
			secondClock.reset();
		}
		if (!collision)
		{
			// Check for collision
			collision = collisionOccurred();
		}
		else
		{
			// Stop the mode if a collision occurred
			std::string score = "Game Over - Score: " + std::to_string(dodged);
			Score->setLabel(score);
		}
	}

	// Mode: Punch the Wall
	if (mode == wall)
	{
		// Change the label of the score button
		std::string score = "Time passed: " + std::to_string(destroyTime) + " ms";
		Score->setLabel(score);
		
		if (!collision)
		{
			// Store the already passed time
			destroyTime = secondClock.getTimeMilliseconds();

			// Check if the wall is destroyed
			collision = wallDestroyed();
		}
		else
		{
			// Stop the mode if a collision occurred
			std::string score = "Game Over - Time passed: " + std::to_string(destroyTime) + " ms";
			Score->setLabel(score);
		}
	}

	// Mode: Let it snow
	if (mode == snow)
	{
		// Call the function "fallingSnow" every 500 ms
		if (secondClock.getTimeMilliseconds() >= 500) 
		{
			// Let snow fall
			fallingSnow();

			// Reset the clock
			secondClock.reset();
		}
	}

	// Renders the display
	renderme();

	// Displays the buttons
	displayButtons();

	// OpenGL: forces execution of GL commands in finite time
	glFlush();

	// changes the front and back buffers
	swapBuffers();
}


/**
* Draws the info text on the info button
*/
void World::drawInfoText()
{
	// Check which info text should be displayed
	if (Mode1->ButtonBoundsTest(this->m_mouseOldX, this->m_mouseOldY))
	{
		Info->setLabel("Stand in front of the kinect and dodge the balls shooted at you");
	}
	else if (Mode2->ButtonBoundsTest(this->m_mouseOldX, this->m_mouseOldY))
	{
		Info->setLabel("Stand in front of the kinect and destroy the wall as fast as you can");
	}
	else if (Mode3->ButtonBoundsTest(this->m_mouseOldX, this->m_mouseOldY))
	{
		Info->setLabel("Stand in front of the kinect and enjoy the snow slowly covering you");
	}
	else if (Score->ButtonBoundsTest(this->m_mouseOldX, this->m_mouseOldY))
	{
		Info->setLabel("Allows you to reconnect your kinect");
	}
	else if (Exit->ButtonBoundsTest(this->m_mouseOldX, this->m_mouseOldY))
	{
		Info->setLabel("Exit the program and return to the desktop");
	}
	else
	{
		Info->setLabel("Hover over a button to see what it does");
	}
}


/**
 * Displays text and buttons on the window
 */
void World::displayButtons()
{
	// Checks if debug mode is enabled and therefore text output is valid
	if ((getDebugMode() & btIDebugDraw::DBG_DrawText) != 0)
	{
		// Sets an orthographic projection to draw the buttons
		setOrthographicProjection();

		// Disables the lighting while drawing the buttons
		glDisable(GL_LIGHTING);

		// Sets the text color
		glColor3f(0, 0, 0);

		// Creates a buffer to hold the text
		char buf[124];

		// Sets the debug text position
		glRasterPos3f(0, 0, 0);
		GLDebugDrawString(0, 0, buf);

		// Check which buttons should be drawn
		if (mode == menu)
		{
			// Draw the menu buttons
			Mode1->ButtonDraw(windowWidth, windowHeight);
			Mode2->ButtonDraw(windowWidth, windowHeight);
			Mode3->ButtonDraw(windowWidth, windowHeight);
			Exit->ButtonDraw(windowWidth, windowHeight);

			// Draw the score button
			Score->ButtonDraw(windowWidth, windowHeight);

			// Draw the info button
			Info->ButtonDraw(windowWidth, windowHeight);
		}
		else
		{
			// Draw the "back to main menu" button
			Back->ButtonDraw(windowWidth, windowHeight);

			// Draw the score button
			Score->ButtonDraw(windowWidth, windowHeight);
		}

		// Resets the perspective after drawing the buttons
		resetPerspectiveProjection();

		// Re-enable the lightning
		glEnable(GL_LIGHTING);
	}
}



/**
 * Responsible for rendering the 3D
 * scene after altering the window
 */
void World::displayCallback(void) {

	// OpenGL: clears buffers to preset values
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Renders the display
	renderme();

	// Displays the buttons
	displayButtons();

	// Check if the world still exists
	if (m_dynamicsWorld)
	{
		// Optional but useful: debug drawing
		m_dynamicsWorld->debugDrawWorld();
	}

	// Updates the window sizes
	windowWidth = this->m_glutScreenWidth;
	windowHeight = this->m_glutScreenHeight;

	// OpenGL: forces execution of GL commands in finite time
	glFlush();

	// changes the front and back buffers
	swapBuffers();
}


/**
 * Initializes physics and
 * sets some world parameters
 */
void World::initPhysics()
{
	// Sets the initial camera perspective
	m_azi = 0;

	// Reset the inner clocks
	firstClock.reset();
	secondClock.reset();

	// Reset the internal flags
	collision = false;
	destroyTime = 0;
	lastSphere = 0;
	dodged = 0;

	// Allows texturing and shadows
	setTexturing(true);
	setShadows(true);

	// Defines the distance needed for two objects to be in contact
	m_defaultContactProcessingThreshold = 0.f;

	// Collision configuration contains default setup for memory, collision setup
	m_collisionConfiguration = new btDefaultCollisionConfiguration();

	// Default collision dispatcher to process the collisions occuring
	m_dispatcher = new	btCollisionDispatcher(m_collisionConfiguration);

	// Needed to calculate collisions of bounding-boxes
	m_broadphase = new btDbvtBroadphase();

	// Default constraint solver to keep relationship between objects correct
	btSequentialImpulseConstraintSolver* sol = new btSequentialImpulseConstraintSolver;
	m_solver = sol;

	// Creates a dynamic 3D scene with the collision dispatcher, the broadphase and the constraint solver
	m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher, m_broadphase, m_solver, m_collisionConfiguration);
	m_dynamicsWorld->getSolverInfo().m_solverMode |= SOLVER_USE_2_FRICTION_DIRECTIONS | SOLVER_RANDMIZE_ORDER;
	m_dynamicsWorld->setDebugDrawer(&sDebugDrawer);

	// Sets the gravity of the world
	m_dynamicsWorld->setGravity(btVector3(0, -10, 0));

	// Create the ground shape
	btCollisionShape* groundShape = new btBoxShape(btVector3(btScalar(110.), btScalar(1.), btScalar(110.)));

	// Create a cube shape
	btCollisionShape* boxShape = new btBoxShape(btVector3(2, 2, 2));

	// Create a sphere shape
	btCollisionShape* sphereShape = new btSphereShape(3);

	// Create a small sphere shape
	btCollisionShape* smallSphereShape = new btSphereShape(1);

	// Create a small cube shape
	btCollisionShape* smallBoxShape = new btBoxShape(btVector3(0.8f, 0.8f, 0.8f));

	// Create a small sphere shape
	btCollisionShape* tinySphereShape = new btSphereShape(0.1f);

	// Add the shapes to the array of stored collision shape objects
	m_collisionShapes.push_back(groundShape);								// m_collisionShapes[0] = groundShape
	m_collisionShapes.push_back(boxShape);									// m_collisionShapes[1] = boxShape
	m_collisionShapes.push_back(sphereShape);								// m_collisionShapes[2] = sphereShape
	m_collisionShapes.push_back(smallSphereShape);							// m_collisionShapes[3] = smallSphereShape
	m_collisionShapes.push_back(smallBoxShape);								// m_collisionShapes[4] = smallBoxShape
	m_collisionShapes.push_back(tinySphereShape);							// m_collisionShapes[5] = tinySphereShape

	// Create Dynamic Objects
	btTransform groundTransform;
	groundTransform.setIdentity();

	// Create a body based on the ground shape
	localCreateRigidBody(0.f, groundTransform, groundShape);

	// Initialize the skeleton
	initSkeleton();
}


/*
* Initializes the skeleton
*/
void World::initSkeleton()
{
	// Loop until 20 objects are created
	for (int i = 0; i < 20; i++)
	{
		// Store the index of the latest rigid body
		reals[i] = m_dynamicsWorld->getNumCollisionObjects();

		// Create Dynamic Objects
		btTransform startTransform;
		startTransform.setIdentity();

		// Choose the sphere as used shape
		btCollisionShape* shape = m_collisionShapes[3];

		// Create a body based on the chosen shape
		btRigidBody* body = localCreateRigidBody(1.f, startTransform, shape);
	}
}


/**
* Resets the scene
*/
void World::clientResetScene()
{
	exitPhysics();
	initPhysics();
}


/**
* Handles keyboard input
* @param key The keyboard key pressed
* @param x Current x-coord of the mouse
* @param y Current y-coord of the mouse
*/
void World::keyboardCallback(unsigned char key, int x, int y)
{
#ifdef GLUT_KEYBOARD_INPUT
	// Check the key
	if (key == '+')
	{
		// Update the camera distance (zoom in)
		cameraDistance -= btScalar(5.);
		setCameraDistance(cameraDistance);
	}
	// Check the key
	if (key == '-')
	{
		// Update the camera distance (zoom out)
		cameraDistance += btScalar(5.);
		setCameraDistance(cameraDistance);
	}
	else
	{
		// Let the system handle the input
		DemoApplication::keyboardCallback(key, x, y);
	}
#endif
}


/**
* Handles mouse input
* @param button The mouse button pressed
* @param state The state of the pressed button
* @param x Current x-coord of the mouse
* @param y Current y-coord of the mouse
*/
void World::mouseFunc(int button, int state, int x, int y)
{
	// If the left mouse button is currently pressed
	if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON)
	{
		if (mode == menu)
		{
			// Check if the mouse is in one buttons bounds
			Mode1->ButtonPress(x, y);
			Mode2->ButtonPress(x, y);
			Mode3->ButtonPress(x, y);
			Score->ButtonPress(x, y);
			Exit->ButtonPress(x, y);
		}
		else
		{
			// Check if the mouse is in the buttons bounds
			Back->ButtonPress(x, y);
		}
	}
	else
	{
		if (mode == menu)
		{
			// Check if the mouse was released in one buttons bounds
			if (Mode1->ButtonRelease(x, y))
			{
				// Reset the scene
				clientResetScene();

				// Set the mode to "sphere"
				mode = sphere;

				// Set the text on the score field
				Score->setLabel("Dodged balls: 0");
			}
			else if (Mode2->ButtonRelease(x, y))
			{
				// Reset the scene
				clientResetScene();

				// Set the mode to "wall"
				mode = wall;

				// Create a wall
				createWall();
			}
			else if (Mode3->ButtonRelease(x, y))
			{
				// Reset the scene
				clientResetScene();

				// Set the mode to "snow"
				mode = snow;

				// Set the text on the score field
				Score->setLabel("Let it snow :)");
			}
			else if (Score->ButtonRelease(x, y))
			{
				// Reinitialize the kinect
				ptr_kinect->kinectStatus = ptr_kinect->initKinect();
			}
			else if (Exit->ButtonRelease(x, y))
			{
				// Button "close" was clicked
				if (ptr_kinect->kinectStatus == S_OK)
				{
					ptr_kinect->shutdownKinect();
				}
				exitPhysics();
				exit(0);
			}
		}
		else
		{
			if (Back->ButtonRelease(x, y))
			{
				// Reset the scene
				clientResetScene();

				// Update the kinect status
				ptr_kinect->kinectStatus = ptr_kinect->initKinect();

				// Push the garbage of player skeleton away
				for (int i = 0; i < 20; i++)
				{
					ptr_kinect->skeleton[i] = btVector3(0, -10, 0);
				}

				// Set the mode to "sphere"
				mode = menu;
			}
		}
	}

	// Force the screen to be redrawn
	glutPostRedisplay();
}


/**
* Shoots a ball at the viewer
*/
void World::shootSphere()
{
	// Check if the world still exists
	if (m_dynamicsWorld)
	{
		// Sets the mass of the sphere
		float mass = 1.f;

		// Store the index of the latest sphere
		lastSphere = m_dynamicsWorld->getNumCollisionObjects();

		// Create Dynamic Objects
		btTransform startTransform;
		startTransform.setIdentity();

		// Choose the sphere as used shape
		btCollisionShape* shape = m_collisionShapes[2];

		// Creates the body to shoot out of the transformed object
		btRigidBody* body = this->localCreateRigidBody(mass, startTransform, shape);
		body->setLinearFactor(btVector3(1, 1, 1));

		// Sets the shooting direction
		btVector3 linVel(randomFloat(2.0) - randomFloat(2.0), 3 + randomFloat(1.0), -10);
		linVel.normalize();
		linVel *= 30;

		// Sets the origin of the object infront of the camera position
		body->getWorldTransform().setOrigin(btVector3(randomFloat(5.0) - randomFloat(5.0), 12 + randomFloat(8.0), 30 - randomFloat(2.0)));
		body->getWorldTransform().setRotation(btQuaternion(0, 0, 0, 1));

		// Aplies the shooting direction
		body->setLinearVelocity(linVel);
		body->setAngularVelocity(btVector3(0, 0, 0));
		body->setContactProcessingThreshold(1e30);
		body->setCcdMotionThreshold(1);
		body->setCcdSweptSphereRadius(0.4f);
	}
}


/* 
 * Creates a wall of box shaped rigid bodies
 */
void World::createWall()
{
	// Check if the world still exists
	if (m_dynamicsWorld)
	{
		// Sets the mass of the boxes
		float mass = 2.f;

		// Create Dynamic Objects
		btTransform startTransform;
		startTransform.setIdentity();

		// Choose the box as used shape
		btCollisionShape* shape = m_collisionShapes[1];

		// Loop until 30 objects are created
		for (int i = 0; i < 30; i++)
		{
			// Store the index of the latest rigid body
			walls[i] = m_dynamicsWorld->getNumCollisionObjects();

			// Creates the body
			btRigidBody* body = this->localCreateRigidBody(mass, startTransform, shape);

			// Sets the origin of the object
			body->getWorldTransform().setOrigin(btVector3(((i % 5) * 4) - 8, (i / 5) * 4, -25));
		}
	}
}


/*
* Creates falling snow
*/
void World::fallingSnow()
{
	// Check if the world still exists
	if (m_dynamicsWorld)
	{
		// Sets the mass of the boxes
		float mass = 1.f;

		// Create Dynamic Objects
		btTransform startTransform;
		startTransform.setIdentity();

		// Choose the tiny sphere as used shape
		btCollisionShape* shape = m_collisionShapes[5];

		// Loop until 10 objects are created
		for (int i = 0; i < 10; i++)
		{
			// Creates the body
			btRigidBody* body = this->localCreateRigidBody(mass, startTransform, shape);

			// Sets the origin of the object
			body->getWorldTransform().setOrigin(btVector3(ptr_kinect->skeleton[i].getX() + randomFloat(50) - randomFloat(50),
				                                          ptr_kinect->skeleton[i].getY() + randomFloat(50) - randomFloat(50) + 5,
				                                          ptr_kinect->skeleton[i].getZ() + randomFloat(50) - randomFloat(50)));
		}
	}
}


/**
* Check for a collision
* @return Whether there occurred a collision
*/
bool World::collisionOccurred()
{
	// Store the sphere
	btCollisionObject* obj1 = m_dynamicsWorld->getCollisionObjectArray()[lastSphere];
	btCollisionObject* obj2 = m_dynamicsWorld->getCollisionObjectArray()[0];

	// Iterate over the Manifolds to get the collisions
	int numManifolds = m_dynamicsWorld->getDispatcher()->getNumManifolds();
	for (int i = 0; i<numManifolds; i++)
	{
		// Iterate over every contact of the Manifold
		btPersistentManifold* contactManifold = m_dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
		int numContacts = contactManifold->getNumContacts();
		for (int j = 0; j<numContacts; j++)
		{
			// For each contact, get the collision point
			btManifoldPoint& pt = contactManifold->getContactPoint(j);
			if (pt.getDistance()<0.0)
			{
				// Check if the groud is not part of the collision
				if (obj1 && contactManifold->getBody0() != obj2 && contactManifold->getBody1() != obj2)
				{
					// Check if the sphere is part of the collision
					if (obj1 && contactManifold->getBody0() == obj1 || contactManifold->getBody1() == obj1)
					{
						// Reset the sphere
						lastSphere = 0;

						// If two collided, return true
						return true;
					}
				}
			}
		}
	}
	// If no collision
	return false;
}


/*
* Check if the wall is destroyed
* @return Whether all upper bodies touch the ground
*/
bool World::wallDestroyed()
{
	// Initialize the result
	bool destroyed = true;

	// Iterate over the upper 10 bodies of the wall
	for (int i = 20; i < 30; i++)
	{
		// Extract the pointer to a body
		btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[walls[i]];
		btRigidBody* body = btRigidBody::upcast(obj);

		// Check if one of the upper bodies is not on the ground
		if (!(body->getWorldTransform().getOrigin().getY() < 3))
		{
			destroyed = false;
		}
	}

	// Return the result
	return destroyed;
}


/**
 * Moves the skeleton based on the cooridnates
 * of a skeletons joints given as array of 3D vectors
 * @param coords: Array of btVector3 coordinates
 */
void World::moveSkeleton(btVector3 coords[], int coordNum)
{
	// Loop over every rigid body
	for (int i = 0; i < coordNum; i++)
	{
		// Extract the pointer to a body
		btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[reals[i]];
		btRigidBody* body = btRigidBody::upcast(obj);

		// Create Dynamic Objects
		btTransform startTransform;
		startTransform.setIdentity();
		startTransform.setOrigin(coords[i]);

		// Move the rigid body
		body->getWorldTransform().setOrigin(coords[i]);
		body->getMotionState()->setWorldTransform(startTransform);
	}
}


/**
* Stops physics and cleans the 3D scene
*/
void World::exitPhysics()
{
	// Remove the rigidbodies from the dynamics world and delete them
	for (int i = m_dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
	{
		btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);
		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}
		m_dynamicsWorld->removeCollisionObject(obj);
		delete obj;
	}

	// Delete collision shapes
	for (int j = 0; j<m_collisionShapes.size(); j++)
	{
		btCollisionShape* shape = m_collisionShapes[j];
		delete shape;
	}

	// Clean up
	m_collisionShapes.clear();
	delete m_dynamicsWorld;
	delete m_solver;
	delete m_broadphase;
	delete m_dispatcher;
	delete m_collisionConfiguration;
}