//////////////////////////////////////////////////////////////////////////////////////////////
//
// Kinect based collision detection
// For "Praktikum: Computerspiele / Special Effects"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a kinect based collision detection between virtual objects 
// and real persons standing in front of the kinect. To achieve the goal, the project is using
// following sources:
//						- GLUT
//						- OpenGL
//						- Bullet Physics
//						- Microsoft Kinect SDK
//
// Credit goes to Rob Bateman for his opengl_ui package 
// And to Erwin Coumans for the C++ Bullet Physics Library.
//
// Authors are Adrian Czarkowski and Holger Honz
// See us also on Bitbucket, where our project is hosted:
// https://bitbucket.org/kinecticats/kinect-based-collision-detection/
//
//////////////////////////////////////////////////////////////////////////////////////////////


#ifndef KINECT_H 
#define KINECT_H 

#include <Windows.h>
#include <NuiApi.h>
#include <NuiImageCamera.h>
#include <NuiSensor.h>
#include "btBulletDynamicsCommon.h" // Contains most common include files


/**
 * Kinect Class:
 * Initializes and connects to the 
 * Kinect and read its sensor data
 */
class Kinect
{
	// Event that will be signaled when skeleton data is available
	HANDLE m_hNextSkeletonEvent;

	// Pointer to the Kinect Sensor
	INuiSensor *sensor;

public:
	// The constructor
	Kinect();

	// The destructor
	~Kinect();

	// Kinect Error Code
	HRESULT kinectStatus;

	// Array of 3D Vectors for the joints of the skeleton
	btVector3 skeleton[20];

	// Initializes the Kinect
	HRESULT initKinect();

	// Shuts down Kinect
	void shutdownKinect();

	// Gets the sensor data and stores it in the intern arrays
	void getNewSkeletonFrame();
};


#endif