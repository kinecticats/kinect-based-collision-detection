//////////////////////////////////////////////////////////////////////////////////////////////
//
// Kinect based collision detection
// For "Praktikum: Computerspiele / Special Effects"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a kinect based collision detection between virtual objects 
// and real persons standing in front of the kinect. To achieve the goal, the project is using
// following sources:
//						- GLUT
//						- OpenGL
//						- Bullet Physics
//						- Microsoft Kinect SDK
//
// Credit goes to Rob Bateman for his opengl_ui package 
// And to Erwin Coumans for the C++ Bullet Physics Library.
//
// Authors are Adrian Czarkowski and Holger Honz
// See us also on Bitbucket, where our project is hosted:
// https://bitbucket.org/kinecticats/kinect-based-collision-detection/
//
//////////////////////////////////////////////////////////////////////////////////////////////


#include "Button.h"
#include "GL\glut.h"

// Define the different positions
enum modes { top_left = 1, top_right = 2, bottom_right = 3, bottom_left = 4, center = 5 };


/**
* Initializes the Button,
* and sets the values
*/
Button::Button(int x, int y, int offsetX, int offsetY, int w, int h, int state, int position, bool highlighted, std::string label)
{
	this->x = x;
	this->y = y;
	this->offsetX = offsetX;
	this->offsetY = offsetY;
	this->w = w;
	this->h = h;
	this->state = state;
	this->position = position;
	this->highlighted = highlighted;
	this->label = label;
}


/**
 * Getter for the Button attributes
 */
int Button::getX() { return x; }
int Button::getY() { return y; }
int Button::getOffsetX() { return offsetX; }
int Button::getOffsetY() { return offsetY; }
int Button::getWidth() { return w; }
int Button::getHeight() { return h; }
int Button::getState() { return state; }
int Button::getPosition() { return position; }
bool Button::isHighlighted() { return highlighted; }
std::string Button::getLabel() { return label; }


/**
* Setter for the Button attributes
*/
void Button::setX(int x) { this->x = x; }
void Button::setY(int y) { this->y = y; }
void Button::setState(int state) { this->state = state; }
void Button::setLabel(std::string label) { this->label = label; }
void Button::setHighlighted(bool highlighted) { this->highlighted = highlighted; }


/**
 * Computes the dimension of a text based on the used font
 */
void Button::Font(void *font, char *text, int x, int y)
{
	glRasterPos2i(x, y);

	while (*text != '\0')
	{
		glutBitmapCharacter(font, *text);
		++text;
	}
}


/**
 * Draws the button on the screen
 * @param windowWidth The current window width
 * @param windowHeight The current window height
 */
void Button::ButtonDraw(int windowWidth, int windowHeight)
{
	// Coordinates of the label
	int fontX;
	int fontY;

	// Check which position the button has
	if (this->getPosition() == top_left)
	{
		// Positions the button based on his offset from the top left corner
		this->setX(this->getOffsetX());
		this->setY(this->getOffsetY());
	}
	else if (this->getPosition() == top_right)
	{
		// Positions the button based on his offset from the top right corner
		this->setX(windowWidth + this->getOffsetX());
		this->setY(this->getOffsetY());
	}
	else if (this->getPosition() == bottom_right)
	{
		// Positions the button based on his offset from the bottom right corner
		this->setX(windowWidth + this->getOffsetX());
		this->setY(windowHeight + this->getOffsetY());
	}
	else if (this->getPosition() == bottom_left)
	{
		// Positions the button based on his offset from the bottom left corner
		this->setX(this->getOffsetX());
		this->setY(windowHeight + this->getOffsetY());
	}
	else if (this->getPosition() == center)
	{
		// Positions the button based on his offset from the center
		this->setX(windowWidth / 2 - this->getWidth() / 2 + this->getOffsetX());
		this->setY(windowHeight / 2 - this->getHeight() / 2 + this->getOffsetY());
	}

	// Color the button, check for need to highlight
	if (this->isHighlighted())
		glColor3f(0.75f, 0.0f, 0.2f);
	else
		glColor3f(0.6f, 0.0f, 0.2f);

	// Draw the background of the button
	glBegin(GL_QUADS);
	glVertex2i(this->getX(), this->getY());
	glVertex2i(this->getX(), this->getY() + this->getHeight());
	glVertex2i(this->getX() + this->getWidth(), this->getY() + this->getHeight());
	glVertex2i(this->getX() + this->getWidth(), this->getY());
	glEnd();

	// Draw an outline around the button with thickness 3
	glLineWidth(3);

	// Reverse the outline color when the button is pressed
	if (this->getState())
		glColor3f(0.4f, 0.4f, 0.4f);
	else
		glColor3f(0.8f, 0.8f, 0.8f);

	// Draw two lines of the outline
	glBegin(GL_LINE_STRIP);
	glVertex2i(this->getX() + this->getWidth(), this->getY());
	glVertex2i(this->getX(), this->getY());
	glVertex2i(this->getX(), this->getY() + this->getHeight());
	glEnd();

	// Reverse the outline color when the button is pressed
	if (this->getState())
		glColor3f(0.8f, 0.8f, 0.8f);
	else
		glColor3f(0.4f, 0.4f, 0.4f);

	// Draw the other two lines of the outline
	glBegin(GL_LINE_STRIP);
	glVertex2i(this->getX(), this->getY() + this->getHeight());
	glVertex2i(this->getX() + this->getWidth(), this->getY() + this->getHeight());
	glVertex2i(this->getX() + this->getWidth(), this->getY());
	glEnd();

	// Draw the font with thickness 1
	glLineWidth(1);

	// Calculate the x and y coords for the text string in order to center it
	fontX = this->getX() + (this->getWidth() - glutBitmapLength(GLUT_BITMAP_HELVETICA_18, (const unsigned char*)this->getLabel().c_str())) / 2;
	fontY = this->getY() + (this->getHeight() + 10) / 2;

	// Change the label position a bit when the button is pressed to obtain a nice visual effect
	if (this->getState()) 
	{
		fontX--;
		fontY--;
	}

	// Set the color of the font
	glColor3f(1, 1, 1);

	// Draw the label
	Font(GLUT_BITMAP_HELVETICA_18, (char*)this->getLabel().c_str(), fontX, fontY);
}


/**
 * Checks if the mouse is within the bounds of the button
 * @param x The current x-coord of the mouse
 * @param y The current y-coord of the mouse
 */
bool Button::ButtonBoundsTest(int x, int y)
{
	// Check if clicked within button bounds
	if (x > this->getX() &&
		x < this->getX() + this->getWidth() &&
		y > this->getY() &&
		y < this->getY() + this->getHeight()) {
		return true;
	}
	else
	{
		return false;
	}
}


/**
 * Checks if the mouse hovers over the button
 * @param x The current x-coord of the mouse
 * @param y The current y-coord of the mouse
 */
void Button::ButtonPassive(int x, int y)
{
	// Check whether the mouse is within the button bounds
	if (ButtonBoundsTest(x, y))
	{
		// Check if the button is already highlighted
		if (!(this->isHighlighted())) 
		{
			// Set the highlighted flag to true
			this->setHighlighted(true);
			
			// Force the screen to be redrawn
			glutPostRedisplay();
		}
	}
	else
	{
		// Check if the button is not hovered but still highlighted
		if (this->isHighlighted())
		{
			// Seth the highlighted flag to false
			this->setHighlighted(false);
			
			// Force the screen to be redrawn
			glutPostRedisplay();
		}
	}
}


/**
 * Checks if the mouse was pressed within the buttons
 * bounds and sets the state of the button to pressed
 * @param x The current x-coord of the mouse
 * @param y The current y-coord of the mouse
 */
void Button::ButtonPress(int x, int y)
{
	// Check if the mouse is within the buttons bounds
	if (ButtonBoundsTest(x, y))
	{
		// Set the state of the button to pressed
		this->setState(1);
	}
}


/**
* Checks if the mouse was released within the buttons
* bounds and sets the state of the button to released
* @param x The current x-coord of the mouse
* @param y The current y-coord of the mouse
*/
bool Button::ButtonRelease(int x, int y)
{
	// Set state back to zero.
	this->setState(0);

	// Checks of the mouse is within the button bounds
	if (ButtonBoundsTest(x, y))
	{
		return true;
	}
	else
	{
		return false;
	}
}