//////////////////////////////////////////////////////////////////////////////////////////////
//
// Kinect based collision detection
// For "Praktikum: Computerspiele / Special Effects"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a kinect based collision detection between virtual objects 
// and real persons standing in front of the kinect. To achieve the goal, the project is using
// following sources:
//						- GLUT
//						- OpenGL
//						- Bullet Physics
//						- Microsoft Kinect SDK
//
// Credit goes to Rob Bateman for his opengl_ui package 
// And to Erwin Coumans for the C++ Bullet Physics Library.
//
// Authors are Adrian Czarkowski and Holger Honz
// See us also on Bitbucket, where our project is hosted:
// https://bitbucket.org/kinecticats/kinect-based-collision-detection/
//
//////////////////////////////////////////////////////////////////////////////////////////////


#ifndef BUTTON
#define BUTTON

#include <string>


/**
* Button Class:
* Allows to create and handle buttons
*/
class Button
{
	int   x;							// Top left x coord of the button
	int   y;							// Top left y coord of the button
	int   offsetX;						// The offset in x from the current position 
	int   offsetY;						// The offset in y from the current position
	int   w;							// The width of the button
	int   h;							// The height of the button
	int	  state;						// The state, 1 if pressed, 0 otherwise
	int   position;						// 1 top-left, 2 top-right, 3 bottom-right, 4 bottom-left, 5 center
	bool  highlighted;					// Is the mouse cursor over the control?
	std::string label;					// The text label of the button

public:
	// The constructor
	Button(int x, int y, int offsetX, int offsetY, int w, int h, int state, int position, bool highlighted, std::string label);

	// The destructor
	virtual ~Button() {};

	// Getter
	int getX();
	int getY();
	int getOffsetX();
	int getOffsetY();
	int getWidth();
	int getHeight();
	int getState();
	int getPosition();
	bool isHighlighted();
	std::string getLabel();

	// Setter
	void setX(int x);
	void setY(int y);
	void setState(int state);
	void setLabel(std::string label);
	void setHighlighted(bool highlighted);

	// Computes the dimension of a text based on the used font
	void Font(void* font, char* text, int x, int y);

	// Draws the button on the screen
	void ButtonDraw(int windowWidth, int windowHeight);

	// Checks if the mouse is within the bounds of a button
	bool ButtonBoundsTest(int x, int y);

	// Checks if the mouse hovers over a button
	void ButtonPassive(int x, int y);

	// Sets the state of a button to <pressed>
	void ButtonPress(int x, int y);

	// Sets the state of a button to <released>
	bool ButtonRelease(int x, int y);
};


#endif BUTTON