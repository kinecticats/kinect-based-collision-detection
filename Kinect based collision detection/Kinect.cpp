//////////////////////////////////////////////////////////////////////////////////////////////
//
// Kinect based collision detection
// For "Praktikum: Computerspiele / Special Effects"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a kinect based collision detection between virtual objects 
// and real persons standing in front of the kinect. To achieve the goal, the project is using
// following sources:
//						- GLUT
//						- OpenGL
//						- Bullet Physics
//						- Microsoft Kinect SDK
//
// Credit goes to Rob Bateman for his opengl_ui package 
// And to Erwin Coumans for the C++ Bullet Physics Library.
//
// Authors are Adrian Czarkowski and Holger Honz
// See us also on Bitbucket, where our project is hosted:
// https://bitbucket.org/kinecticats/kinect-based-collision-detection/
//
//////////////////////////////////////////////////////////////////////////////////////////////


#include "Kinect.h"
#include <iostream> // For debugging purpose on the console


/**
* Initializes the Kinect
*/
Kinect::Kinect()
{
	kinectStatus = E_FAIL;
	sensor = NULL;
}


/**
 * Shuts down the sensor and
 * stops the skeleton stream
 */
Kinect::~Kinect()
{
	// Checks if sensor is connected
	if (sensor)
	{
		// Shuts the sensor down
		sensor->NuiShutdown();
	}
	// Checks if the skeleton handler is valid
	if (m_hNextSkeletonEvent && (m_hNextSkeletonEvent != INVALID_HANDLE_VALUE))
	{
		// Stops the skeleton stream
		CloseHandle(m_hNextSkeletonEvent);
	}

	// Initialize the skeleton array with 0 vectors
	for (int i = 0; i < 20; i++) 
	{
		skeleton[i] = btVector3(0, 0, 0);
	}
}


/**
 * Connects the sensor, and sets specific options
 * @return: HRESULT - ErrorCode
 */
HRESULT Kinect::initKinect()
{
	// Instantiate a sensor
	INuiSensor* nuiSensor;

	// Set an sensor count to check multiple Kinects
	int sensorCount = 0;
	HRESULT hr = NuiGetSensorCount(&sensorCount);

	// If failed to set one, return error
	if (FAILED(hr))
	{
		return hr;
	}

	// Look at all possible Kinect sensors
	for (int i = 0; i < 6; ++i)
	{
		// Create a sensor to check its status
		hr = NuiCreateSensorByIndex(i, &nuiSensor);

		
		// If failed to set the sensor, continue
		if (FAILED(hr))
		{
			continue;
			
		}

		// Get the status of the sensor and initialize it if connected
		hr = nuiSensor->NuiStatus();
		if (S_OK == hr)
		{
			sensor = nuiSensor;
			break;
		}

		// Release the unused temporary sensor
		nuiSensor->Release();
	}

	// Set options if a sensor was connected
	if (NULL != sensor)
	{
		// Initialize the Kinect and specify that we'll be using skeleton
		hr = sensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_SKELETON);
		if (SUCCEEDED(hr))
		{
			// Create an event that will be signaled when skeleton data is available
			m_hNextSkeletonEvent = CreateEventW(NULL, TRUE, FALSE, NULL);

			// Open a skeleton stream to receive skeleton data
			hr = sensor->NuiSkeletonTrackingEnable(m_hNextSkeletonEvent, 0);
		}
	}

	// Return an error if no sensor was connected
	if (NULL == sensor || FAILED(hr))
	{
		return E_FAIL;
	}
	nuiSensor->NuiCameraElevationSetAngle(10);
	return hr;
}


/**
* Shuts down the sensor, and brings in back in standard position
*/
void Kinect::shutdownKinect()
{
	// Angle to 0
	sensor->NuiCameraElevationSetAngle(0);
	// Shut down sensor
	sensor->NuiShutdown();
}


/**
 * Gets the sensor data and
 * stores it in the intern arrays
 */
void Kinect::getNewSkeletonFrame()
{
	// Leave if no sensor connected
	if (NULL == sensor)
	{
		return;
	}

	// Check if it is time to process a skeleton
	if (WAIT_OBJECT_0 == WaitForSingleObject(m_hNextSkeletonEvent, 0))
	{
		// Instantiate an empty skeleton frame
		NUI_SKELETON_FRAME skeletonFrame = { 0 };

		// Get the next skeleton frame
		HRESULT hr = sensor->NuiSkeletonGetNextFrame(0, &skeletonFrame);

		// Leave if failed to load the next frame
		if (FAILED(hr))
		{
			return;
		}

		// Iterate over every 6 skeletons
		for (int i = 0; i < NUI_SKELETON_COUNT; i++)
		{
			// Get the tracking state for the first skeleton
			NUI_SKELETON_TRACKING_STATE trackingState = skeletonFrame.SkeletonData[i].eTrackingState;

			// Store the 20 joint coordinates in intern arrays
			for (int j = 0; j < 20; j++) {

				// Store its joints as coordinates if the skeleton is tracked
				if (NUI_SKELETON_TRACKED == trackingState)
				{
					skeleton[j] = btVector3(- (skeletonFrame.SkeletonData[i].SkeletonPositions[j].x * 8),
											   skeletonFrame.SkeletonData[i].SkeletonPositions[j].y * 8 + 10,
											- (skeletonFrame.SkeletonData[i].SkeletonPositions[j].z * 5 + 20));
				}
			}
		}
	}
}