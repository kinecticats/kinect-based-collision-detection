//////////////////////////////////////////////////////////////////////////////////////////////
//
// Kinect based collision detection
// For "Praktikum: Computerspiele / Special Effects"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a kinect based collision detection between virtual objects 
// and real persons standing in front of the kinect. To achieve the goal, the project is using
// following sources:
//						- GLUT
//						- OpenGL
//						- Bullet Physics
//						- Microsoft Kinect SDK
//
// Credit goes to Rob Bateman for his opengl_ui package 
// And to Erwin Coumans for the C++ Bullet Physics Library.
//
// Authors are Adrian Czarkowski and Holger Honz
// See us also on Bitbucket, where our project is hosted:
// https://bitbucket.org/kinecticats/kinect-based-collision-detection/
//
//////////////////////////////////////////////////////////////////////////////////////////////


#include "World.h"
#include "Kinect.h"
#include "Windows.h"
#include "GlutStuff.h"
#include "GLDebugDrawer.h"
#include "btBulletDynamicsCommon.h" // Contains most common include files

// Create a debug drawer
GLDebugDrawer gDebugDrawer;


int main(int argc, char** argv)
{
	// Instantiate a new Kinect
	Kinect* kinect = new Kinect();
	kinect->kinectStatus = kinect->initKinect();

	// Instantiate a new 3D space
	World world;

	// Associate the kinect with the world
	world.associateKinect(kinect);
	
	// Initialize physics and the debug drawer in the world
	world.initPhysics();
	world.getDynamicsWorld()->setDebugDrawer(&gDebugDrawer);

	// Create a window showing the instantiated 3D space
	return glutmain(argc, argv, 1024, 600, "Kinect based collision detection", &world);

	// Clean up
	delete kinect;
	delete &world;
	return 0;
}